package net.ilgrim.hotels.api.Database;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Alex B on 21.07.2015.
 */
public class ConnectOfDB {
    public Connection getConnect() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/api", "postgres", "0000");

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return null;
    }
}
