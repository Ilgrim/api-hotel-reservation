package net.ilgrim.hotels.api.Database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Cursor {
    private final List<Map<String, Object>> storage;

    private int position = (-1);

    public Cursor(ResultSet result) {
        storage = new ArrayList<>();

        if (result == null) return;

        try { // TODO возможно оптимизировать
            final ResultSetMetaData meta = result.getMetaData();

            while (result.next()) {
                final Map<String, Object> row = new HashMap<>();

                for (int i = 1; i <= meta.getColumnCount(); i++) {
                    row.put(meta.getColumnName(i), result.getObject(i));
                }

                storage.add(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean next() {
        if (position == (-1) && !storage.isEmpty()) {
            position = 0;
            return true;
        }

        if (position >= 0) {
            position++;
            return position < storage.size();
        }

        return false;
    }

    public int size() {
        return storage.size();
    }

    public int getInt(String name, int defValue) {
        final Object obj = storage.get(position).get(name);

        if (obj == null) return defValue;
        return (Integer) obj;
    }

    public long getLong(String name, long defValue) {
        final Object obj = storage.get(position).get(name);

        if (obj == null) return defValue;
        return (Long) obj;
    }

    public String getString(String name) {
        return getString(name, null);
    }

    public String getString(String name, String defValue) {
        final Object obj = storage.get(position).get(name);

        if (obj == null) return defValue;
        return ((String) obj).trim();
    }

    public boolean getBoolean(String name, boolean defValue) {
        final Object obj = storage.get(position).get(name);

        if (obj == null) return defValue;
        return (Boolean) obj;
    }

    public float getFloat(String name, float defValue) {
        final Object obj = storage.get(position).get(name);

        if (obj == null) return defValue;
        return Float.valueOf(obj.toString());
    }

    public Timestamp getTimestamp(String name, String defValue) {
        final Object obj = storage.get(position).get(name);

        if (obj == null) return Timestamp.valueOf(defValue);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaNueva = null;
        try {
            fechaNueva = format.parse(obj.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Timestamp.valueOf(format.format(fechaNueva));
    }

    public Object getObject(String name, Object defValue) {
        final Object obj = storage.get(position).get(name);

        if (obj == null) return defValue;

        return obj;
    }
}
