package net.ilgrim.hotels.api.Database;

import java.sql.*;

public class DatabaseHelper {
    private final Connection connection;

    public DatabaseHelper(Connection connection) {
        this.connection = connection;
    }

    public Cursor query(String sql, Object... params) {
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }

            result = statement.executeQuery();
            return new Cursor(result);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
            closeResultSet(result);
        }

        return new Cursor(null);
    }

    public int insert(String sql, Object... params) {
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
        }

        return 0;
    }

    public int update(String sql, Object... params) {
        return insert(sql, params);
    }

    public int delete(String sql, Object... params) {
        return insert(sql, params);
    }

    private void closeStatement(Statement statement) {
        if (statement == null) return;

        try {
            if (statement.isClosed()) return;
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeResultSet(ResultSet result) {
        if (result == null) return;

        try {
            if (result.isClosed()) return;
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}