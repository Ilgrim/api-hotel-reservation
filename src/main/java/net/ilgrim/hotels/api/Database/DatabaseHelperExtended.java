package net.ilgrim.hotels.api.Database;

import org.postgresql.util.PSQLException;

import java.sql.*;

/**
 * Created by ilgrim on 26.11.15.
 */
public class DatabaseHelperExtended extends DatabaseHelper {
    private final Connection connection;

    public DatabaseHelperExtended(Connection connection) {
        super(connection);
        this.connection = connection;
    }


    public int insertWithReturnId(String sql, Object... params) {

        PreparedStatement statement = null;
        ResultSet rsKey = null;
        try {
            statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            statement.executeUpdate();

            rsKey = statement.getGeneratedKeys();

            if(rsKey.next()) {
                return rsKey.getInt("id");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
            closeResultSet(rsKey);
        }

        return 0;
    }

    public Cursor insertWithReturnCursor(String sql, Object... params) {

        PreparedStatement statement = null;
        ResultSet rsKey = null;
        try {
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }

            statement.execute();

            rsKey = statement.getResultSet();

            return new Cursor(rsKey);

        } catch (PSQLException e) {
            return new Cursor(null);
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            closeStatement(statement);
            closeResultSet(rsKey);
        }

        return new Cursor(null);
    }

    public Cursor updateWithReturnCursor(String sql, Object... params) {
        return insertWithReturnCursor(sql, params);
    }

    private void closeStatement(Statement statement) {
        if (statement == null) return;

        try {
            if (statement.isClosed()) return;
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeResultSet(ResultSet result) {
        if (result == null) return;

        try {
            if (result.isClosed()) return;
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
