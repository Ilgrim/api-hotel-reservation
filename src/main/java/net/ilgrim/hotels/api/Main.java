package net.ilgrim.hotels.api;

import net.ilgrim.hotels.api.Database.ConnectOfDB;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.filters.AfterFilter;
import net.ilgrim.hotels.api.filters.AuthFilter;
import net.ilgrim.hotels.api.routes.auth.AuthPost;
import net.ilgrim.hotels.api.routes.comforts.HotelComfortsGet;
import net.ilgrim.hotels.api.routes.comforts.comfort.HotelComfortDelete;
import net.ilgrim.hotels.api.routes.comforts.comfort.HotelComfortGet;
import net.ilgrim.hotels.api.routes.comforts.comfort.HotelComfortPatch;
import net.ilgrim.hotels.api.routes.comforts.comfort.HotelComfortPost;
import net.ilgrim.hotels.api.routes.comforts.in.hotel.CHotelDelete;
import net.ilgrim.hotels.api.routes.comforts.in.hotel.CHotelGet;
import net.ilgrim.hotels.api.routes.comforts.in.hotel.CHotelPatch;
import net.ilgrim.hotels.api.routes.comforts.in.hotel.CHotelPost;
import net.ilgrim.hotels.api.routes.comforts.in.room.CRoomDelete;
import net.ilgrim.hotels.api.routes.comforts.in.room.CRoomGet;
import net.ilgrim.hotels.api.routes.comforts.in.room.CRoomPatch;
import net.ilgrim.hotels.api.routes.comforts.in.room.CRoomPost;
import net.ilgrim.hotels.api.routes.hotels.HotelsGet;
import net.ilgrim.hotels.api.routes.hotels.hotel.HotelDelete;
import net.ilgrim.hotels.api.routes.hotels.hotel.HotelGet;
import net.ilgrim.hotels.api.routes.hotels.hotel.HotelPatch;
import net.ilgrim.hotels.api.routes.hotels.hotel.HotelPost;
import net.ilgrim.hotels.api.routes.hotels.hotel.comments.*;
import net.ilgrim.hotels.api.routes.hotels.hotel.coordinates.*;
import net.ilgrim.hotels.api.routes.hotels.hotel.period.*;
import net.ilgrim.hotels.api.routes.hotels.hotel.photo.*;
import net.ilgrim.hotels.api.routes.hotels.hotel.rooms.HotelRoomsGet;
import net.ilgrim.hotels.api.routes.locations.LocationsGet;
import net.ilgrim.hotels.api.routes.locations.location.LocationDelete;
import net.ilgrim.hotels.api.routes.locations.location.LocationGet;
import net.ilgrim.hotels.api.routes.locations.location.LocationPatch;
import net.ilgrim.hotels.api.routes.locations.location.LocationPost;
import net.ilgrim.hotels.api.routes.placements.PlacemetsGet;
import net.ilgrim.hotels.api.routes.placements.placement.PlacementDelete;
import net.ilgrim.hotels.api.routes.placements.placement.PlacementGet;
import net.ilgrim.hotels.api.routes.placements.placement.PlacementPatch;
import net.ilgrim.hotels.api.routes.placements.placement.PlacementPost;
import net.ilgrim.hotels.api.routes.rooms.Room.RoomDelete;
import net.ilgrim.hotels.api.routes.rooms.Room.RoomGet;
import net.ilgrim.hotels.api.routes.rooms.Room.RoomPatch;
import net.ilgrim.hotels.api.routes.rooms.Room.RoomPost;
import net.ilgrim.hotels.api.routes.rooms.Room.photo.*;
import net.ilgrim.hotels.api.routes.rooms.Room.price.*;
import net.ilgrim.hotels.api.routes.rooms.RoomsGet;
import net.ilgrim.hotels.api.routes.rooms.category.CategoriesGet;
import net.ilgrim.hotels.api.routes.rooms.category.CategoryDelete;
import net.ilgrim.hotels.api.routes.rooms.category.CategoryPatch;
import net.ilgrim.hotels.api.routes.rooms.category.CategoryPost;
import net.ilgrim.hotels.api.routes.rooms.comforts.RoomsComfortsGet;
import net.ilgrim.hotels.api.routes.rooms.comforts.comfort.RoomComfortDelete;
import net.ilgrim.hotels.api.routes.rooms.comforts.comfort.RoomComfortGet;
import net.ilgrim.hotels.api.routes.rooms.comforts.comfort.RoomComfortPatch;
import net.ilgrim.hotels.api.routes.rooms.comforts.comfort.RoomComfortPost;
import net.ilgrim.hotels.api.routes.scheme.SchemeRoute;

import java.sql.Connection;

import static spark.Spark.*;

/**
 * Created by ilgrim.
 */
public class Main {

    public static void main(String[] a) {
        final Connection connection = new ConnectOfDB().getConnect();
        final DatabaseHelperExtended databaseHelperExtended = new DatabaseHelperExtended(connection);

        port(8888);

        //TODO написать ответы для всех интерфейсов
        //TODO написать получение всех данных из таблиц
        //TODO написать интерфейс поиска


        before("/api/v1/*", (request, response) -> {
            if (request.requestMethod().equals("OPTIONS")) {
                response.header("Access-Control-Allow-Origin", "*");
                response.header("Access-Control-Allow-Methods", "DELETE");
                response.header("Access-Control-Allow-Methods", "PATCH");
                halt(200);
            }
        });

        get("/api/v1/schemes", new SchemeRoute(databaseHelperExtended));

        post("/api/v1/user/auth", new AuthPost(databaseHelperExtended));

        before("/api/v1/*", new AuthFilter(databaseHelperExtended));

        /**Locations**/
        get("/api/v1/locations", new LocationsGet(databaseHelperExtended));
        /**END**/

        /**Location**/
        post("/api/v1/location", new LocationPost(databaseHelperExtended));
        get("/api/v1/location/:id", new LocationGet(databaseHelperExtended));
        delete("/api/v1/location/:id", new LocationDelete(databaseHelperExtended));
        patch("/api/v1/location/:id", new LocationPatch(databaseHelperExtended));
        /**END**/

        /**Type placements**/
        get("/api/v1/placements", new PlacemetsGet(databaseHelperExtended));
            /**Type placement**/
            post("/api/v1/placement", new PlacementPost(databaseHelperExtended));
            get("/api/v1/placement/:id", new PlacementGet(databaseHelperExtended));
            delete("/api/v1/placement/:id", new PlacementDelete(databaseHelperExtended));
            patch("/api/v1/placement/:id", new PlacementPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Hotels comforts**/
        get("/api/v1/comforts", new HotelComfortsGet(databaseHelperExtended));
            /**Hotel comforts**/
            post("/api/v1/comfort", new HotelComfortPost(databaseHelperExtended));
            get("/api/v1/comfort/:id", new HotelComfortGet(databaseHelperExtended));
            delete("/api/v1/comfort/:id", new HotelComfortDelete(databaseHelperExtended));
            patch("/api/v1/comfort/:id", new HotelComfortPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Comforts in Hotels**/
        post("/api/v1/comforts/in/hotel", new CHotelPost(databaseHelperExtended));
        get("/api/v1/comforts/in/hotel", new CHotelGet(databaseHelperExtended));
        delete("/api/v1/comforts/in/hotel/:id", new CHotelDelete(databaseHelperExtended));
        patch("/api/v1/comforts/in/hotel/:id", new CHotelPatch(databaseHelperExtended));
        /**END**/

        /**Hotel photos**/
        get("/api/v1/hotels/photo", new HotelPhotosGet(databaseHelperExtended));
            /**Hotel photo**/
            post("/api/v1/hotel/:hotel_id/photo", new HotelPhotoPost(databaseHelperExtended));
            get("/api/v1/hotel/:hotel_id/photo", new HotelPhotoGet(databaseHelperExtended));
            delete("/api/v1/hotel/photo/:id", new HotelPhotoDelete(databaseHelperExtended));
            patch("/api/v1/hotel/:hotel_id/photo/:id", new HotelPhotoPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Hotels coordinates**/
        get("/api/v1/hotels/coordinates", new HotelsCoordinatesGet(databaseHelperExtended));
            /**Hotel coordinates**/
            post("/api/v1/hotel/:hotel_id/coordinates", new HotelCoordinatesPost(databaseHelperExtended));
            get("/api/v1/hotel/:hotel_id/coordinates", new HotelCoordinatesGet(databaseHelperExtended));
            delete("/api/v1/hotel/coordinates/:id", new HotelCoordinatesDelete(databaseHelperExtended));
            patch("/api/v1/hotel/:hotel_id/coordinates/:id", new HotelCoordinatesPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Hotel comments**/
        get("/api/v1/hotels/comments", new HotelsCommentsGet(databaseHelperExtended));
            /**Hotel comments**/
            post("/api/v1/hotel/:hotel_id/comment", new HotelCommentPost(databaseHelperExtended));
            get("/api/v1/hotel/:hotel_id/comment", new HotelCommentsGet(databaseHelperExtended));
            delete("/api/v1/comment/:id", new HotelCommentDelete(databaseHelperExtended));
            patch("/api/v1/comment/:id", new HotelCommentPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Hotels**/
        get("/api/v1/hotels", new HotelsGet(databaseHelperExtended));
            /**Hotel**/
            post("/api/v1/hotel", new HotelPost(databaseHelperExtended));
            get("/api/v1/hotel/:id", new HotelGet(databaseHelperExtended));
            delete("/api/v1/hotel/:id", new HotelDelete(databaseHelperExtended));
            patch("/api/v1/hotel/:id", new HotelPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Periods**/
        get("/api/v1/hotels/period", new HotelsPeriodsGet(databaseHelperExtended));
            /**Period**/
            get("/api/v1/hotel/:hotel_id/period", new HotelPeriodsGet(databaseHelperExtended));
            post("/api/v1/hotel/:hotel_id/period", new HotelPeriodPost(databaseHelperExtended));
            delete("/api/v1/hotel/period/:id", new HotelPeriodDelete(databaseHelperExtended));
            patch("/api/v1/hotel/:hotel_id/period/:id", new HotelPeriodPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Categories**/
        get("/api/v1/categories", new CategoriesGet(databaseHelperExtended));
            /**Categories**/
            post("/api/v1/category", new CategoryPost(databaseHelperExtended));
            patch("/api/v1/category/:id", new CategoryPatch(databaseHelperExtended));
            delete("/api/v1/category/:id", new CategoryDelete(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Rooms photo**/
        get("/api/v1/rooms/photo", new RoomsPhotoGet(databaseHelperExtended));
            /**Room photo**/
            post("/api/v1/room/:room_id/photo", new RoomPhotoPost(databaseHelperExtended));
            get("/api/v1/room/:room_id/photo", new RoomPhotoGet(databaseHelperExtended));
            delete("/api/v1/room/photo/:id", new RoomPhotoDelete(databaseHelperExtended));
            patch("/api/v1/room/:room_id/photo/:id", new RoomPhotoPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Rooms comforts**/
        get("/api/v1/rooms/comforts", new RoomsComfortsGet(databaseHelperExtended));
            /**Room comforts**/
            post("/api/v1/rooms/comfort", new RoomComfortPost(databaseHelperExtended));
            get("/api/v1/rooms/comfort/:id", new RoomComfortGet(databaseHelperExtended));
            delete("/api/v1/rooms/comfort/:id", new RoomComfortDelete(databaseHelperExtended));
            patch("/api/v1/rooms/comfort/:id", new RoomComfortPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Comforts in Rooms**/
        post("/api/v1/comforts/in/room", new CRoomPost(databaseHelperExtended));
        get("/api/v1/comforts/in/room", new CRoomGet(databaseHelperExtended));
        delete("/api/v1/comforts/in/room/:id", new CRoomDelete(databaseHelperExtended));
        patch("/api/v1/comforts/in/room/:id", new CRoomPatch(databaseHelperExtended));
        /**END**/

        /**Rooms**/
        get("/api/v1/rooms", new RoomsGet(databaseHelperExtended));
        get("/api/v1/hotel/:hotel_id/rooms", new HotelRoomsGet(databaseHelperExtended));
            /**Room**/
            post("/api/v1/room", new RoomPost(databaseHelperExtended));
            get("/api/v1/room/:id", new RoomGet(databaseHelperExtended));
            delete("/api/v1/room/:id", new RoomDelete(databaseHelperExtended));
            patch("/api/v1/room/:id", new RoomPatch(databaseHelperExtended));
            /**END**/
        /**END**/

        /**Price**/
        get("/api/v1/room/period/prices", new PricesGet(databaseHelperExtended));
            /**Price**/
            post("/api/v1/room/:room_id/period/:period_id/price", new PricePost(databaseHelperExtended));
            get("/api/v1/room/period/price/:id", new PriceGet(databaseHelperExtended));
            delete("/api/v1/room/period/price/:id", new PriceDelete(databaseHelperExtended));
            patch("/api/v1/room/:room_id/period/:period_id/price/:id", new PricePatch(databaseHelperExtended));
            /**END**/
        /**END**/

        after("/api/v1/*", new AfterFilter());
    }
}
