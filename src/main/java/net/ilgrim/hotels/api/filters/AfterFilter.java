package net.ilgrim.hotels.api.filters;

import spark.Filter;
import spark.Request;
import spark.Response;

/**
 * Created by ilgrim.
 */
public class AfterFilter implements Filter {
    @Override
    public void handle(Request request, Response response) throws Exception {
        response.header("Content-type", "application/json");
    }
}
