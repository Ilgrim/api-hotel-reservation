package net.ilgrim.hotels.api.filters;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.routes.RouteResponses;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

/**
 * Created by ilgrim.
 */
public class AuthFilter extends RouteResponses implements Filter {
    private final DatabaseHelperExtended database;

    public AuthFilter(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public void handle(Request request, Response response) throws Exception {
        response.header("Access-Control-Allow-Origin", "*");

        System.out.println(request.requestMethod());
        final String token = request.queryParams("token");

        switch (request.pathInfo()) {
            case "/api/v1/schemes":
                break;
            case "/api/v1/user/auth":
                break;
            default:
                if (!request.requestMethod().equals("GET")) {
                    block(token);
                }
        }
    }

    private void block(String token) {

        final Cursor checkToken = database.query("SELECT token FROM user_tokens WHERE token = ?;", token);

        if (checkToken.size() == 0) {
            System.out.println(2);
            Spark.halt(401, "You are not welcome here!");
        }
    }
}
