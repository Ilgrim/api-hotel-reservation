package net.ilgrim.hotels.api.models;

import net.ilgrim.hotels.api.models.meta.Meta;

public class Rjson<T> {
    private final Meta meta;
    private final T content;

    public Rjson(Meta meta, T content) {
        this.meta = meta;
        this.content = content;
    }
}
