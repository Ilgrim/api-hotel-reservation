package net.ilgrim.hotels.api.models;

public class Status {
    private final int code;
    private final String message;

    public Status(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
