package net.ilgrim.hotels.api.models.meta;

public class Error {
    private final int code;
    private final String message;

    public Error(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
