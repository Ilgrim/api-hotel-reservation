package net.ilgrim.hotels.api.models.meta;

import net.ilgrim.hotels.api.models.Status;

import java.util.List;

public class Meta {
    private final Status status;
    private final List<Error> errors;

    public Meta(Status status) {
        this(status, null);
    }

    public Meta(Status status, List<Error> errors) {
        this.status = status;
        this.errors = errors;
    }
}
