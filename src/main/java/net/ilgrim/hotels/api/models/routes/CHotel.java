package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim.
 */
public class CHotel {
    private int id;
    private int hotel_id;
    private int comfort_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(int hotel_id) {
        this.hotel_id = hotel_id;
    }

    public int getComfort_id() {
        return comfort_id;
    }

    public void setComfort_id(int comfort_id) {
        this.comfort_id = comfort_id;
    }
}
