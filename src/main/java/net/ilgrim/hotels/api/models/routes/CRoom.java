package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim.
 */
public class CRoom {
    private int id;
    private int room_id;
    private int comfort_room_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public int getComfort_room_id() {
        return comfort_room_id;
    }

    public void setComfort_room_id(int comfort_room_id) {
        this.comfort_room_id = comfort_room_id;
    }
}
