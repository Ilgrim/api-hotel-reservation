package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim.
 */
public class Comfort {
    private int id;
    private String title;

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
