package net.ilgrim.hotels.api.models.routes;

import java.sql.Timestamp;

/**
 * Created by ilgrim.
 */
public class Comments {
    private int id;
    private int hotel_id;
    private int assessment;
    private String author;
    private String comment;
    private Timestamp date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHotelId() {
        return hotel_id;
    }

    public void setHotelId(int hotelId) {
        this.hotel_id = hotelId;
    }

    public int getAssessment() {
        return assessment;
    }

    public void setAssessment(int assessment) {
        this.assessment = assessment;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
