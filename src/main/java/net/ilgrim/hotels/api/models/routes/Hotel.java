package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim.
 */
public class Hotel {
    private int id;
    private String title;
    private String description;
    private int location_id;
    private int type_placement_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public int getType_placement_id() {
        return type_placement_id;
    }

    public void setType_placement_id(int type_placement_id) {
        this.type_placement_id = type_placement_id;
    }
}
