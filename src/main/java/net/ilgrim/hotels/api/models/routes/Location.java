package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim on 26.11.15.
 */
public class Location {
    private int id;
    private String title;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
