package net.ilgrim.hotels.api.models.routes;

import java.sql.Timestamp;

/**
 * Created by ilgrim.
 */
public class Period {
    private int id;
    private int hotel_id;
    private String title_period;
    private Timestamp period_start;
    private Timestamp period_end;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHotel_id() {
        return hotel_id;
    }

    public void setHotelId(int hotel_id) {
        this.hotel_id = hotel_id;
    }

    public Timestamp getPeriod_start() {
        return period_start;
    }

    public void setPeriod_start(Timestamp period_start) {
        this.period_start = period_start;
    }

    public Timestamp getPeriod_end() {
        return period_end;
    }

    public void setPeriod_end(Timestamp period_end) {
        this.period_end = period_end;
    }

    public String getTitle_period() {
        return title_period;
    }

    public void setTitlePeriod(String title_period) {
        this.title_period = title_period;
    }
}
