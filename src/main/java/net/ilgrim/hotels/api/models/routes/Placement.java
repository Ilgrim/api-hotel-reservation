package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim on 27.11.15.
 */
public class Placement {
    private int id;
    private String placement;

    public void setId(int id) {
        this.id = id;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }
}
