package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim.
 */
public class Price {
    int id;
    int period_id;
    int room_id;
    int cost_standart;
    int cost_child;
    int cost_extra_standart;
    int cost_extra_child;
    int day_off_cost_standart;
    int day_off_cost_child;
    int day_off_cost_extra_standart;
    int day_off_cost_extra_child;
    boolean stock;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeriod_id() {
        return period_id;
    }

    public void setPeriodId(int period_id) {
        this.period_id = period_id;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoomId(int room_id) {
        this.room_id = room_id;
    }

    public int getCost_standart() {
        return cost_standart;
    }

    public void setCostStandart(int cost_standart) {
        this.cost_standart = cost_standart;
    }

    public int getCost_child() {
        return cost_child;
    }

    public void setCostChild(int cost_child) {
        this.cost_child = cost_child;
    }

    public int getCost_extra_standart() {
        return cost_extra_standart;
    }

    public void setCostExtraStandart(int cost_extra_standart) {
        this.cost_extra_standart = cost_extra_standart;
    }

    public int getCost_extra_child() {
        return cost_extra_child;
    }

    public void setCostExtraChild(int cost_extra_child) {
        this.cost_extra_child = cost_extra_child;
    }

    public int getDay_off_cost_standart() {
        return day_off_cost_standart;
    }

    public void setDayOffCostStandart(int day_off_cost_standart) {
        this.day_off_cost_standart = day_off_cost_standart;
    }

    public int getDay_off_cost_child() {
        return day_off_cost_child;
    }

    public void setDayOffCostChild(int day_off_cost_child) {
        this.day_off_cost_child = day_off_cost_child;
    }

    public int getDay_off_cost_extra_standart() {
        return day_off_cost_extra_standart;
    }

    public void setDayOffCostExtraStandart(int day_off_cost_extra_standart) {
        this.day_off_cost_extra_standart = day_off_cost_extra_standart;
    }

    public int getDay_off_cost_extra_child() {
        return day_off_cost_extra_child;
    }

    public void setDayOffCostExtraChild(int day_off_cost_extra_child) {
        this.day_off_cost_extra_child = day_off_cost_extra_child;
    }

    public boolean isStock() {
        return stock;
    }

    public void setStock(boolean stock) {
        this.stock = stock;
    }
}
