package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim.
 */
public class Room {
    private int id;
    private int hotel_id;
    private int number_beds;
    private int number_extra_beds;
    private int category_id;
    private String title;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(int hotel_id) {
        this.hotel_id = hotel_id;
    }

    public int getNumber_beds() {
        return number_beds;
    }

    public void setNumber_beds(int number_beds) {
        this.number_beds = number_beds;
    }

    public int getNumber_extra_beds() {
        return number_extra_beds;
    }

    public void setNumber_extra_beds(int number_extra_beds) {
        this.number_extra_beds = number_extra_beds;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
