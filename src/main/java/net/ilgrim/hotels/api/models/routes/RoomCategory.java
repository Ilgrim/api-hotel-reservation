package net.ilgrim.hotels.api.models.routes;

/**
 * Created by ilgrim.
 */
public class RoomCategory {
    private int id;
    private String category;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
