package net.ilgrim.hotels.api.models.routes.auth;

/**
 * Created by ilgrim.
 */
public class UserToken {
    /*private int id;
    private int user_id;*/
    private String token;

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }*/

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
