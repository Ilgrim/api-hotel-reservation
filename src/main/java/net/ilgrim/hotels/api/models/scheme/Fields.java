package net.ilgrim.hotels.api.models.scheme;

import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class Fields {
    private String name;
    private String label;
    private String type;
    private ArrayList<String> values;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public void setValues(ArrayList<String> values) {
        this.values = values;
    }
}
