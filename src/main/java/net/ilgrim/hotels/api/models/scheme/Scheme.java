package net.ilgrim.hotels.api.models.scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class Scheme {
    private String titleInset;
    private HashMap<String, String> methods;
    private ArrayList<HashMap<String, Object>> fields;

    public String getTitleInset() {
        return titleInset;
    }

    public void setTitleInset(String titleInset) {
        this.titleInset = titleInset;
    }

    public HashMap<String, String> getMethods() {
        return methods;
    }

    public void setMethods(HashMap<String, String> methods) {
        this.methods = methods;
    }

    public ArrayList<HashMap<String, Object>> getFields() {
        return fields;
    }

    public void setFields(ArrayList<HashMap<String, Object>> fields) {
        this.fields = fields;
    }
}
