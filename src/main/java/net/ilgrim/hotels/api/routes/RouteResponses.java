package net.ilgrim.hotels.api.routes;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Error;
import net.ilgrim.hotels.api.models.meta.Meta;
import spark.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilgrim.
 */
public class RouteResponses {

    protected String emptyField() {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(1, "empty field"));
        final Status status = new Status(400, "incorrect data");
        final Meta meta = new Meta(status, errors);
        final Rjson<String> rjson = new Rjson<>(meta, null);

        return new Gson().toJson(rjson);
    }

    protected String errorWritingDatabase() {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(2, "error writing to database"));
        final Status status = new Status(500, "database error");
        final Meta meta = new Meta(status, errors);
        final Rjson<String> rjson = new Rjson<>(meta, null);
        return new Gson().toJson(rjson);
    }

    protected String errorDeleteData(Response response) {
        response.status(500);

        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(7, "error delete data"));
        final Status status = new Status(500, "database error");
        final Meta meta = new Meta(status, errors);
        final Rjson<String> rjson = new Rjson<>(meta, null);
        return new Gson().toJson(rjson);
    }

    protected String errorReadDatabase() {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(3, "error read to database"));
        final Status status = new Status(500, "database error");
        final Meta meta = new Meta(status, errors);
        final Rjson<String> rjson = new Rjson<>(meta, null);
        return new Gson().toJson(rjson);
    }

    protected String dataContains() {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(4, "Found match the input data with the data in the database"));
        final Status status = new Status(400, "data match found");
        final Meta meta = new Meta(status, errors);
        final Rjson<String> rjson = new Rjson<>(meta, null);
        return new Gson().toJson(rjson);
    }

    protected String incorrectData(String field) {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(5, "Field: "+field+" not a number"));
        final Status status = new Status(400, "incorrect data");
        final Meta meta = new Meta(status, errors);
        final Rjson<String> rjson = new Rjson<>(meta, null);

        return new Gson().toJson(rjson);
    }

    protected String zeeroInId() {
        final List<Error> errors = new ArrayList<>();
        errors.add(new Error(6, "param == 0"));
        final Status status = new Status(400, "zero in id");
        final Meta meta = new Meta(status, errors);
        final Rjson<String> rjson = new Rjson<>(meta, null);

        return new Gson().toJson(rjson);
    }
}
