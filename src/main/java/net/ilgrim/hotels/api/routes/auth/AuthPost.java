package net.ilgrim.hotels.api.routes.auth;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.auth.User;
import net.ilgrim.hotels.api.models.routes.auth.UserToken;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.Util;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Created by ilgrim.
 */
public class AuthPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public AuthPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        response.header("Content-type", "application/json");

        if (request.queryParams().size() < 2) return emptyField();

        final User user = new User();
        for (String param : request.queryParams()) {
            switch (param) {
                case "login":
                    user.setUsername(request.queryParams(param));
                    continue;
                case "password":
                    user.setPassword(Util.sha1(request.queryParams(param)));
            }
        }

        final Cursor cursor = database.query("SELECT id FROM users WHERE username = ? AND password = ?;", user.getUsername(), user.getPassword());
        if (cursor.size() == 0) {
            return errorReadDatabase();
        }

        cursor.next();
        final int userId = cursor.getInt("id", 0);

        final String tokenString = Util.sha1(user.getUsername() + userId + user.getPassword());

        final Cursor checkToken = database.query("SELECT token FROM user_tokens WHERE token = ?;", tokenString);

        if (checkToken.size() == 0) {
            final Cursor token = database.insertWithReturnCursor("INSERT INTO user_tokens (user_id, token) VALUES (?, ?) RETURNING *;",
                    cursor.getInt("id", 0), tokenString);

            if (token.size() == 0) {
                return errorWritingDatabase();
            }
        }

        final UserToken userToken = new UserToken();
        userToken.setToken(tokenString);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<UserToken> rjson = new Rjson<>(meta, userToken);
        return new Gson().toJson(rjson);
    }
}
