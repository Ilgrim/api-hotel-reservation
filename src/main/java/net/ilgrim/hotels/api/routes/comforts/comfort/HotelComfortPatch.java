package net.ilgrim.hotels.api.routes.comforts.comfort;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Comfort;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelComfortPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelComfortPatch(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int comfortId = Integer.parseInt(request.params("id"));

        if (comfortId == 0) {
            return zeeroInId();
        }

        final String comfortTitle = URLDecoder.decode(request.queryParams("title"), "UTF-8");
        if (comfortTitle.equals("")) {
            return emptyField();
        }

        final int changes = database.insert("UPDATE comforts set title = ? where id = ?;", comfortTitle, comfortId);

        if (changes == 0) {
            errorWritingDatabase();
        }

        final Cursor cursor = database.query("SELECT id, title FROM comforts WHERE id = ?", comfortId);

        if (cursor.size() == 0) {
            return errorReadDatabase();
        }

        final ArrayList<Comfort> comforts = ModelCreator.create(new Comfort(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Comfort>> rjson = new Rjson<>(meta, comforts);
        return new Gson().toJson(rjson);
    }
}
