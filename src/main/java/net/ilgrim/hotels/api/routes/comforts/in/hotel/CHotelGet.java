package net.ilgrim.hotels.api.routes.comforts.in.hotel;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.CHotel;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class CHotelGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public CHotelGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        final Cursor cursor = database.query("SELECT id, hotel_id, comfort_id FROM comfort_in_hotel;");

        final ArrayList<CHotel> cHotels = ModelCreator.create(new CHotel(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<CHotel>> rjson = new Rjson<>(meta, cHotels);
        return new Gson().toJson(rjson);
    }
}
