package net.ilgrim.hotels.api.routes.comforts.in.hotel;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.CHotel;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class CHotelPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public CHotelPatch(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int id = Integer.parseInt(request.params("id"));

        if (id == 0) {
            return zeeroInId();
        }

        final CHotel cHotel = new CHotel();
        try {
            cHotel.setHotel_id(Integer.parseInt(request.queryParams("hotel_id")));
        } catch (NumberFormatException e) {
            return incorrectData("hotel_id");
        }

        try {
            cHotel.setComfort_id(Integer.parseInt(request.queryParams("comfort_id")));
        } catch (NumberFormatException e) {
            return incorrectData("comfort_id");
        }

        final Cursor changes = database.updateWithReturnCursor("UPDATE comfort_in_hotel set hotel_id = ?, comfort_id = ? where id = ? RETURNING *;", cHotel.getHotel_id(), cHotel.getComfort_id(), id);

        if (changes.size() == 0) {
            errorWritingDatabase();
        }

        final ArrayList<CHotel> cHotels = ModelCreator.create(new CHotel(), changes);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<CHotel>> rjson = new Rjson<>(meta, cHotels);
        return new Gson().toJson(rjson);
    }
}
