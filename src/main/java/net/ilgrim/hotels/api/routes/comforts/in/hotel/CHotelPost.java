package net.ilgrim.hotels.api.routes.comforts.in.hotel;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.CHotel;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class CHotelPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public CHotelPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final CHotel cHotelModel = new CHotel();
        try {
            cHotelModel.setHotel_id(Integer.parseInt(request.queryParams("hotel_id")));
        } catch (NumberFormatException e) {
            return incorrectData("hotel_id");
        }

        try {
            cHotelModel.setComfort_id(Integer.parseInt(request.queryParams("comfort_id")));
        } catch (NumberFormatException e) {
            return incorrectData("comfort_id");
        }

        final Cursor cursor = database.insertWithReturnCursor("INSERT INTO comfort_in_hotel (hotel_id, comfort_id) VALUES (?, ?) RETURNING *;", cHotelModel.getHotel_id(), cHotelModel.getComfort_id());
        if (cursor.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<CHotel> cHotels = ModelCreator.create(new CHotel(), cursor);
        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<CHotel>> rjson = new Rjson<>(meta, cHotels);
        return new Gson().toJson(rjson);
    }
}
