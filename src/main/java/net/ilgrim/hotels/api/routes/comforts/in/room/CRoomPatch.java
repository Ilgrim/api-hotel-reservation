package net.ilgrim.hotels.api.routes.comforts.in.room;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.CRoom;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class CRoomPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public CRoomPatch(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int id = Integer.parseInt(request.params("id"));

        if (id == 0) {
            return zeeroInId();
        }

        final CRoom cRoom = new CRoom();
        try {
            cRoom.setRoom_id(Integer.parseInt(request.queryParams("room_id")));
        } catch (NumberFormatException e) {
            return incorrectData("room_id");
        }

        try {
            cRoom.setComfort_room_id(Integer.parseInt(request.queryParams("comfort_room_id")));
        } catch (NumberFormatException e) {
            return incorrectData("comfort_room_id");
        }

        final Cursor changes = database.updateWithReturnCursor("UPDATE comforts_in_room set room_id = ?, comfort_room_id = ? where id = ? RETURNING *;", cRoom.getRoom_id(), cRoom.getComfort_room_id(), id);

        if (changes.size() == 0) { //TODO проверка, должно изменить только одну запись
            errorWritingDatabase();
        }

        final ArrayList<CRoom> cRooms = ModelCreator.create(new CRoom(), changes);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<CRoom>> rjson = new Rjson<>(meta, cRooms);
        return new Gson().toJson(rjson);
    }
}
