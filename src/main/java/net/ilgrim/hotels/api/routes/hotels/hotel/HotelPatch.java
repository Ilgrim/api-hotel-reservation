package net.ilgrim.hotels.api.routes.hotels.hotel;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Hotel;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPatch(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int hotelId = Integer.parseInt(request.params("id"));

        if (hotelId == 0) {
            return zeeroInId();
        }

        if (request.queryParams().size() < 4) return emptyField();
        final Hotel hotel = new Hotel();
        for (String param : request.queryParams()) {
            switch (param) {
                case "title":
                    hotel.setTitle(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "description":
                    hotel.setDescription(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "location_id":
                    try {
                        hotel.setLocation_id(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
                    continue;
                case "type_placement_id":
                    try {
                        hotel.setType_placement_id(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
            }
        }

        final Cursor updateHotel = database.updateWithReturnCursor("UPDATE hotels set title = ?, description = ?, location_id = ?, type_placement_id = ? where id = ? RETURNING *;", hotel.getTitle(), hotel.getDescription(), hotel.getLocation_id(), hotel.getType_placement_id(), hotelId);
        if (updateHotel.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<Hotel> hotels = ModelCreator.create(new Hotel(), updateHotel);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Hotel>> rjson = new Rjson<>(meta, hotels);
        return new Gson().toJson(rjson);
    }
}
