package net.ilgrim.hotels.api.routes.hotels.hotel;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Hotel;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        if (request.queryParams().size() < 4) return emptyField();

        final Hotel hotel = new Hotel();
        for (String param : request.queryParams()) {
            switch (param) {
                case "title":
                    hotel.setTitle(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "description":
                    hotel.setDescription(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "location_id":
                    try {
                        hotel.setLocation_id(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
                    continue;
                case "type_placement_id":
                    try {
                        hotel.setType_placement_id(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
            }
        }

        final Cursor checkHotel = database.query("SELECT id, title FROM hotels WHERE lower(title) = lower(?);", hotel.getTitle());
        if (checkHotel.size() > 0) {
            return dataContains();
        }

        final Cursor newHotel = database.insertWithReturnCursor("INSERT INTO hotels (title, description, location_id, type_placement_id) VALUES (?, ?, ?, ?) RETURNING *;", hotel.getTitle(), hotel.getDescription(), hotel.getLocation_id(), hotel.getType_placement_id());
        if (newHotel.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<Hotel> hotels = ModelCreator.create(new Hotel(), newHotel);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Hotel>> rjson = new Rjson<>(meta, hotels);
        return new Gson().toJson(rjson);
    }
}
