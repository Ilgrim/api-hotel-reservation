package net.ilgrim.hotels.api.routes.hotels.hotel.comments;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Comments;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HotelCommentPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelCommentPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final String author = URLDecoder.decode(request.queryParams("author"), "UTF-8");
        if (author.equals("")) {
            return emptyField();
        }
        final String comment = URLDecoder.decode(request.queryParams("comment"), "UTF-8");
        if (comment.equals("")) {
            return emptyField();
        }

        try {
            getDateTime(request.queryParams("date"));
        } catch (NumberFormatException e) {
            return incorrectData("date");

        }
        final Timestamp date = getDateTime(request.queryParams("date"));

        try {
            Integer.parseInt(request.queryParams("assessment"));
        } catch (NumberFormatException e) {
            return incorrectData("assessment");

        }
        final long assessment = Integer.parseInt(request.queryParams("assessment"));


        try {
            Integer.parseInt(request.params("hotel_id"));
        } catch (NumberFormatException e) {
            return incorrectData("hotel_id");

        }
        final int hotelId = Integer.parseInt(request.params("hotel_id"));
        if (hotelId == 0) {
            return zeeroInId();
        }



        final Cursor hotelComments = database.insertWithReturnCursor("INSERT INTO comments (hotel_id, author, comment, date, assessment) VALUES (?, ?, ?, ?, ?) RETURNING *;", hotelId, author, comment, date, assessment);

        if (hotelComments.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<Comments> comments = ModelCreator.create(new Comments(), hotelComments);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Comments>> rjson = new Rjson<>(meta, comments);
        return new Gson().toJson(rjson);
    }

    private Timestamp getDateTime(String obj) {
        try {
            obj = URLDecoder.decode(obj, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            final Date fechaNueva = format.parse(obj);
            return Timestamp.valueOf(format.format(fechaNueva));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}