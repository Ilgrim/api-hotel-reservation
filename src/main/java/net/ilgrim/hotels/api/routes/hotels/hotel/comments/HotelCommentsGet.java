package net.ilgrim.hotels.api.routes.hotels.hotel.comments;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Comments;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelCommentsGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelCommentsGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int hotelId = Integer.parseInt(request.params("id"));

        if (hotelId == 0) {
            return zeeroInId();
        }

        final Cursor cursor = database.query("SELECT id, author, comment, date, assessment FROM comments WHERE hotel_id = ?;", hotelId);

        final ArrayList<Comments> comments = ModelCreator.create(new Comments(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Comments>> rjson = new Rjson<>(meta, comments);
        return new Gson().toJson(rjson);
    }
}
