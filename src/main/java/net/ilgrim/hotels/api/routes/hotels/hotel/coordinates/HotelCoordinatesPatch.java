package net.ilgrim.hotels.api.routes.hotels.hotel.coordinates;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Coordinates;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelCoordinatesPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelCoordinatesPatch(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        final String latitude = URLDecoder.decode(request.queryParams("latitude"), "UTF-8");
        if (latitude.equals("")) {
            return emptyField();
        }
        final String longitude = URLDecoder.decode(request.queryParams("longitude"), "UTF-8");
        if (longitude.equals("")) {
            return emptyField();
        }

        try {
            Integer.parseInt(request.params("hotel_id"));
        } catch (NumberFormatException e) {
            return incorrectData("hotel_id");

        }
        final int hotelId = Integer.parseInt(request.params("hotel_id"));
        if (hotelId == 0) {
            return zeeroInId();
        }

        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int coordinatesId = Integer.parseInt(request.params("id"));

        if (coordinatesId == 0) {
            return zeeroInId();
        }

        final Cursor changes = database.updateWithReturnCursor("UPDATE coordinates set hotel_id = ?, latitude = ?, longitude = ? where id = ? RETURNING *;", hotelId, latitude, longitude, coordinatesId);

        if (changes.size() == 0) {
            errorWritingDatabase();
        }

        final ArrayList<Coordinates> coordinates = ModelCreator.create(new Coordinates(), changes);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Coordinates>> rjson = new Rjson<>(meta, coordinates);
        return new Gson().toJson(rjson);
    }
}
