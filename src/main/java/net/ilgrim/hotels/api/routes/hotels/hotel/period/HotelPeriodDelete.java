package net.ilgrim.hotels.api.routes.hotels.hotel.period;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.routes.RouteResponses;
import spark.Request;
import spark.Response;
import spark.Route;

public class HotelPeriodDelete extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPeriodDelete(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int periodId = Integer.parseInt(request.params("id"));

        if (periodId == 0) {
            return zeeroInId();
        }

        int changes = database.delete("DELETE FROM periods WHERE id = ?;", periodId);

        if (changes == 0) {
            return errorDeleteData(response);
        }

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<String> rjson = new Rjson<>(meta, null);
        return new Gson().toJson(rjson);
    }
}
