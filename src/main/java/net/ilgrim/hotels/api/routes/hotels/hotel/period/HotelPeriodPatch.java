package net.ilgrim.hotels.api.routes.hotels.hotel.period;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Period;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelPeriodPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPeriodPatch(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int periodId = Integer.parseInt(request.params("id"));

        if (periodId == 0) {
            return zeeroInId();
        }

        try {
            Integer.parseInt(request.params("hotel_id"));
        } catch (NumberFormatException e) {
            return incorrectData("hotel_id");
        }

        final int hotelId = Integer.parseInt(request.params("hotel_id"));

        if (hotelId == 0) {
            return zeeroInId();
        }

        if (request.queryParams().size() < 3) return emptyField();

        final Period period = new Period();
        for (String param : request.queryParams()) {
            switch (param) {
                case "title_period":
                    period.setTitlePeriod(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "period_start":

                    try {
                        period.setPeriod_start(getDateTime(request.queryParams("period_start")));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
                    continue;
                case "period_end":
                    try {
                        period.setPeriod_end(getDateTime(request.queryParams("period_end")));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
            }
        }

        final Cursor updatePeriod = database.updateWithReturnCursor("UPDATE periods set title_period = ?, period_start = ?, period_end = ?, hotel_id = ? where id = ? RETURNING *;", period.getTitle_period(), period.getPeriod_start(), period.getPeriod_end(), hotelId, periodId);
        if (updatePeriod.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<Period> periods = ModelCreator.create(new Period(), updatePeriod);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Period>> rjson = new Rjson<>(meta, periods);
        return new Gson().toJson(rjson);
    }

    private Timestamp getDateTime(String obj) {
        try {
            obj = URLDecoder.decode(obj, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            final Date fechaNueva = format.parse(obj);
            return Timestamp.valueOf(format.format(fechaNueva));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
