package net.ilgrim.hotels.api.routes.hotels.hotel.period;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Period;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HotelPeriodPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPeriodPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("hotel_id"));
        } catch (NumberFormatException e) {
            return incorrectData("hotel_id");
        }

        int hotelId = Integer.parseInt(request.params("hotel_id"));

        if (hotelId == 0) {
            return zeeroInId();
        }

        if (request.queryParams().size() < 3) return emptyField();

        final Period period = new Period();
        for (String param : request.queryParams()) {
            switch (param) {
                case "title_period":
                    period.setTitlePeriod(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "period_start":
                    try {
                        period.setPeriod_start(getDateTime(request.queryParams("period_start")));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
                    continue;
                case "period_end":
                    try {
                        period.setPeriod_end(getDateTime(request.queryParams("period_end")));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
            }
        }

        final Cursor checkPeriod = database.query("SELECT id, title_period FROM periods WHERE lower(title_period) = lower(?);", period.getTitle_period());
        if (checkPeriod.size() > 0) {
            return dataContains();
        }

        final Cursor newPeriod = database.insertWithReturnCursor("INSERT INTO periods (hotel_id, title_period, period_start, period_end) VALUES (?, ?, ?, ?) RETURNING *;", hotelId, period.getTitle_period(), period.getPeriod_start(), period.getPeriod_end());
        if (newPeriod.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<Period> periods = ModelCreator.create(new Period(), newPeriod);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Period>> rjson = new Rjson<>(meta, periods);
        return new Gson().toJson(rjson);
    }

    private Timestamp getDateTime(String obj) {
        try {
            obj = URLDecoder.decode(obj, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            final Date fechaNueva = format.parse(obj);
            return Timestamp.valueOf(format.format(fechaNueva));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
