package net.ilgrim.hotels.api.routes.hotels.hotel.period;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Period;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelPeriodsGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPeriodsGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        int hotelId = Integer.parseInt(request.params("id"));

        final Cursor cursor = database.query("SELECT id, hotel_id, title_period, period_start, period_end FROM periods WHERE hotel_id = ?;", hotelId);

        final ArrayList<Period> periods = ModelCreator.create(new Period(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Period>> rjson = new Rjson<>(meta, periods);
        return new Gson().toJson(rjson);
    }
}
