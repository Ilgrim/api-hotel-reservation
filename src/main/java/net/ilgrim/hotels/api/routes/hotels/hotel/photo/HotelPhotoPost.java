package net.ilgrim.hotels.api.routes.hotels.hotel.photo;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.HotelPhoto;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

public class HotelPhotoPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPhotoPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final String photoUrl = URLDecoder.decode(request.queryParams("photo_url"), "UTF-8");
        if (photoUrl.equals("")) {
            return emptyField();
        }

        try {
            Integer.parseInt(request.params("hotel_id"));
        } catch (NumberFormatException e) {
            return incorrectData("hotel_id");

        }
        final int hotelId = Integer.parseInt(request.params("hotel_id"));
        if (hotelId == 0) {
            return zeeroInId();
        }

        final Cursor photo = database.insertWithReturnCursor("INSERT INTO hotel_photo_urls (hotel_id, photo_url) VALUES (?, ?) RETURNING *;", hotelId, photoUrl);

        if (photo.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<HotelPhoto> hotelPhotos = ModelCreator.create(new HotelPhoto(), photo);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<HotelPhoto>> rjson = new Rjson<>(meta, hotelPhotos);
        return new Gson().toJson(rjson);
    }
}