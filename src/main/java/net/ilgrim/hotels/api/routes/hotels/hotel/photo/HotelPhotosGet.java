package net.ilgrim.hotels.api.routes.hotels.hotel.photo;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.HotelPhoto;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class HotelPhotosGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public HotelPhotosGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final Cursor cursor = database.query("SELECT id, hotel_id, photo_url FROM hotel_photo_urls;");

        final ArrayList<HotelPhoto> hotelPhotos = ModelCreator.create(new HotelPhoto(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<HotelPhoto>> rjson = new Rjson<>(meta, hotelPhotos);
        return new Gson().toJson(rjson);
    }
}
