package net.ilgrim.hotels.api.routes.locations.location;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Location;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class LocationGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public LocationGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        int locationId = Integer.parseInt(request.params("id"));

        if (locationId == 0) {
            return zeeroInId();
        }

        final Cursor cursor = database.query("SELECT id, title FROM locations WHERE id = ?;", locationId);

        final ArrayList<Location> locations = ModelCreator.create(new Location(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Location>> rjson = new Rjson<>(meta, locations);
        return new Gson().toJson(rjson);
    }
}
