package net.ilgrim.hotels.api.routes.locations.location;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Location;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

public class LocationPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public LocationPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final String title = URLDecoder.decode(request.queryParams("title"), "UTF-8");
        if (title.equals("")) {
            return emptyField();
        }

        final Cursor checkData = database.query("SELECT id, title FROM locations WHERE lower(title) = lower(?);", title);

        if (checkData.size() > 0) {
            return dataContains();
        }

        final int locationId = database.insertWithReturnId("INSERT INTO locations (title) VALUES (?);", title);

        if (locationId == 0) {
            return errorWritingDatabase();
        }

        final Cursor dataRespons = database.query("SELECT id, title FROM locations WHERE id = ?", locationId);

        if (dataRespons.size() == 0) {
            return errorReadDatabase();
        }

        final ArrayList<Location> locations = ModelCreator.create(new Location(), dataRespons);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Location>> rjson = new Rjson<>(meta, locations);
        return new Gson().toJson(rjson);
    }
}