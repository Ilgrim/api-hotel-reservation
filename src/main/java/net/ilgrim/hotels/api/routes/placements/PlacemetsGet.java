package net.ilgrim.hotels.api.routes.placements;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Placement;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 27.11.15.
 */
public class PlacemetsGet extends RouteResponses implements Route{
    private final DatabaseHelperExtended database;

    public PlacemetsGet(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public Object handle(Request request, Response response) throws Exception {
        final Cursor cursor = database.query("SELECT id, placement FROM type_placement;");

        final ArrayList<Placement> placements = ModelCreator.create(new Placement(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Placement>> rjson = new Rjson<>(meta, placements);
        return new Gson().toJson(rjson);
    }

}
