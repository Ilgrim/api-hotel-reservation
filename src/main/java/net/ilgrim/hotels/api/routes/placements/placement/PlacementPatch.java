package net.ilgrim.hotels.api.routes.placements.placement;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Placement;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class PlacementPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public PlacementPatch(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int placementId = Integer.parseInt(request.params("id"));

        if (placementId == 0) {
            return zeeroInId();
        }

        final String placementTitle = URLDecoder.decode(request.queryParams("placement"), "UTF-8");
        if (placementTitle.equals("")) {
            return emptyField();
        }

        final int changes = database.update("UPDATE type_placement set placement = ? where id = ?;", placementTitle, placementId);

        if (changes == 0) {
            errorWritingDatabase();
        }


        final Cursor cursor = database.query("SELECT id, placement FROM type_placement WHERE id = ?", placementId);

        if (cursor.size() == 0) {
            return errorReadDatabase();
        }

        final ArrayList<Placement> placements = ModelCreator.create(new Placement(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Placement>> rjson = new Rjson<>(meta, placements);
        return new Gson().toJson(rjson);
    }
}
