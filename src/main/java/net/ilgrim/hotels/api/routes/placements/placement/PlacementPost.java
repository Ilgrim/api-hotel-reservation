package net.ilgrim.hotels.api.routes.placements.placement;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Placement;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

public class PlacementPost  extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public PlacementPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final String placementTitle = URLDecoder.decode(request.queryParams("placement"), "UTF-8");
        if (placementTitle.equals("")) {
            return emptyField();
        }

        final Cursor checkData = database.query("SELECT id, placement FROM type_placement WHERE lower(placement) = lower(?);", placementTitle);

        if (checkData.size() > 0) {
            return dataContains();
        }

        final int placementId = database.insertWithReturnId("INSERT INTO type_placement (placement) VALUES (?);", placementTitle);

        if (placementId == 0) {
            return errorWritingDatabase();
        }

        final Cursor dataRespons = database.query("SELECT id, placement FROM type_placement WHERE id = ?", placementId);

        if (dataRespons.size() == 0) {
            return errorReadDatabase();
        }

        final ArrayList<Placement> placements = ModelCreator.create(new Placement(), dataRespons);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Placement>> rjson = new Rjson<>(meta, placements);
        return new Gson().toJson(rjson);
    }
}