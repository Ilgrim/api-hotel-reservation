package net.ilgrim.hotels.api.routes.rooms.Room;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Room;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class RoomGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public RoomGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        int roomId = Integer.parseInt(request.params("id"));

        if (roomId == 0) {
            return zeeroInId();
        }

        final Cursor cursor = database.query("SELECT * FROM hotel_room WHERE id = ?;", roomId);

        final ArrayList<Room> rooms = ModelCreator.create(new Room(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Room>> rjson = new Rjson<>(meta, rooms);
        return new Gson().toJson(rjson);
    }
}
