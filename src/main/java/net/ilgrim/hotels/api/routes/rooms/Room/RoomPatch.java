package net.ilgrim.hotels.api.routes.rooms.Room;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Room;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class RoomPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public RoomPatch(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int roomId = Integer.parseInt(request.params("id"));

        if (roomId == 0) {
            return zeeroInId();
        }

        if (request.queryParams().size() < 6) return emptyField();
        final Room room = new Room();
        for (String param : request.queryParams()) {
            switch (param) {
                case "title":
                    room.setTitle(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "description":
                    room.setDescription(URLDecoder.decode(request.queryParams(param), "UTF-8"));
                    continue;
                case "category_id":
                    try {
                        room.setCategory_id(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
                    continue;
                case "hotel_id":
                    try {
                        room.setHotel_id(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
                case "number_beds":
                    try {
                        room.setNumber_beds(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
                case "number_extra_beds":
                    try {
                        room.setNumber_extra_beds(Integer.parseInt(request.queryParams(param)));
                    } catch (NumberFormatException e) {
                        return incorrectData(param);
                    }
            }
        }

        final Cursor updateRoom = database.updateWithReturnCursor("UPDATE hotel_room set title = ?, description = ?, category_id = ?, hotel_id = ?, number_beds = ?, number_extra_beds = ? where id = ? RETURNING *;", room.getTitle(), room.getDescription(), room.getCategory_id(), room.getHotel_id(), room.getNumber_beds(), room.getNumber_extra_beds(), roomId);
        if (updateRoom.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<Room> rooms = ModelCreator.create(new Room(), updateRoom);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Room>> rjson = new Rjson<>(meta, rooms);
        return new Gson().toJson(rjson);
    }
}
