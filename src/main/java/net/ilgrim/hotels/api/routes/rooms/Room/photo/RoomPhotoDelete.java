package net.ilgrim.hotels.api.routes.rooms.Room.photo;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.routes.RouteResponses;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Created by ilgrim on 25.11.15.
 */
public class RoomPhotoDelete extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public RoomPhotoDelete(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int photoId = Integer.parseInt(request.params("id"));

        if (photoId == 0) {
            return zeeroInId();
        }

        final int changes = database.delete("DELETE FROM room_photo_urls WHERE id = ?;", photoId);
        if (changes == 0) {
            return errorDeleteData(response);
        }

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<String> rjson = new Rjson<>(meta, null);
        return new Gson().toJson(rjson);
    }
}
