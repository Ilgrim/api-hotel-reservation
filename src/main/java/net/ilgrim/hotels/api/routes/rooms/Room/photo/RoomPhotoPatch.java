package net.ilgrim.hotels.api.routes.rooms.Room.photo;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.RoomPhoto;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class RoomPhotoPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public RoomPhotoPatch(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int photoId = Integer.parseInt(request.params("id"));

        if (photoId == 0) {
            return zeeroInId();
        }

        try {
            Integer.parseInt(request.params("room_id"));
        } catch (NumberFormatException e) {
            return incorrectData("room_id");

        }
        final int roomId = Integer.parseInt(request.params("room_id"));
        if (roomId == 0) {
            return zeeroInId();
        }

        final String photoUrl = URLDecoder.decode(request.queryParams("photo_url"), "UTF-8");
        if (photoUrl.equals("")) {
            return emptyField();
        }

        final Cursor changes = database.updateWithReturnCursor("UPDATE room_photo_urls set photo_url = ?, room_id = ? where id = ? RETURNING *;", photoUrl, roomId, photoId);

        if (changes.size() == 0) { //TODO проверка, должно изменить только одну запись
            errorWritingDatabase();
        }

        final ArrayList<RoomPhoto> roomPhotos = ModelCreator.create(new RoomPhoto(), changes);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<RoomPhoto>> rjson = new Rjson<>(meta, roomPhotos);
        return new Gson().toJson(rjson);
    }
}
