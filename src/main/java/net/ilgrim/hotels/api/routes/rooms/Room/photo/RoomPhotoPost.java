package net.ilgrim.hotels.api.routes.rooms.Room.photo;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.RoomPhoto;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

public class RoomPhotoPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public RoomPhotoPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final String photoUrl = URLDecoder.decode(request.queryParams("photo_url"), "UTF-8");
        if (photoUrl.equals("")) {
            return emptyField();
        }

        try {
            Integer.parseInt(request.params("room_id"));
        } catch (NumberFormatException e) {
            return incorrectData("room_id");

        }
        final int roomId = Integer.parseInt(request.params("room_id"));
        if (roomId == 0) {
            return zeeroInId();
        }

        final Cursor photo = database.insertWithReturnCursor("INSERT INTO room_photo_urls (room_id, photo_url) VALUES (?, ?) RETURNING *;", roomId, photoUrl);

        if (photo.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<RoomPhoto> roomPhotos = ModelCreator.create(new RoomPhoto(), photo);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<RoomPhoto>> rjson = new Rjson<>(meta, roomPhotos);
        return new Gson().toJson(rjson);
    }
}