package net.ilgrim.hotels.api.routes.rooms.Room.price;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Price;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class PriceGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public PriceGet(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {

        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int id = Integer.parseInt(request.params("id"));

        if (id == 0) {
            return zeeroInId();
        }

        final Cursor cursor = database.query("SELECT * FROM price_period WHERE id = ?;", id);

        final ArrayList<Price> prices = ModelCreator.create(new Price(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Price>> rjson = new Rjson<>(meta, prices);
        return new Gson().toJson(rjson);
    }
}
