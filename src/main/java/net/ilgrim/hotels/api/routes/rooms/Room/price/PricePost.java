package net.ilgrim.hotels.api.routes.rooms.Room.price;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Price;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim on 25.11.15.
 */
public class PricePost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public PricePost(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        if (request.queryParams().size() < 9) return emptyField();

        try {
            Integer.parseInt(request.params("room_id"));
        } catch (NumberFormatException e) {
            return incorrectData("room_id");
        }

        final int roomId = Integer.parseInt(request.params("room_id"));

        if (roomId == 0) {
            return zeeroInId();
        }

        try {
            Integer.parseInt(request.params("period_id"));
        } catch (NumberFormatException e) {
            return incorrectData("period_id");
        }

        final int periodId = Integer.parseInt(request.params("period_id"));

        if (periodId == 0) {
            return zeeroInId();
        }

        final Price price = new Price();

        for (String param : request.queryParams()) {
            switch (param) {
                case "cost_standart":
                    price.setCostStandart(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "cost_child":
                    price.setCostChild(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "cost_extra_standart":
                    price.setCostExtraStandart(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "cost_extra_child":
                    price.setCostExtraChild(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "day_off_cost_standart":
                    price.setDayOffCostStandart(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "day_off_cost_child":
                    price.setDayOffCostChild(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "day_off_cost_extra_standart":
                    price.setDayOffCostExtraStandart(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "day_off_cost_extra_child":
                    price.setDayOffCostExtraChild(Integer.parseInt(request.queryParams(param)));
                    continue;
                case "stock":
                    price.setStock(Boolean.valueOf(request.queryParams(param)));
            }
        }

        final Cursor newPrice = database
                .insertWithReturnCursor(
                        "INSERT INTO price_period (room_id, period_id, cost_standart, cost_child, cost_extra_standart, cost_extra_child, day_off_cost_standart, day_off_cost_child, day_off_cost_extra_standart, day_off_cost_extra_child, stock) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *;",
                        roomId, periodId, price.getCost_standart(), price.getCost_child(), price.getCost_extra_standart(), price.getCost_extra_child(), price.getDay_off_cost_standart(), price.getDay_off_cost_child(), price.getDay_off_cost_extra_standart(), price.getDay_off_cost_extra_child(), price.isStock()
                );
        if (newPrice.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<Price> prices = ModelCreator.create(new Price(), newPrice);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Price>> rjson = new Rjson<>(meta, prices);
        return new Gson().toJson(rjson);
    }
}
