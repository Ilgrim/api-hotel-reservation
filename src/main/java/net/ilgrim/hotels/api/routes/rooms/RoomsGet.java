package net.ilgrim.hotels.api.routes.rooms;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Room;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

public class RoomsGet extends RouteResponses implements Route {

    private final DatabaseHelperExtended database;

    public RoomsGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final Cursor cursor = database.query("SELECT id, hotel_id, title, description, category_id, number_beds, number_extra_beds FROM hotel_room;");

        final ArrayList<Room> rooms = ModelCreator.create(new Room(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Room>> rjson = new Rjson<>(meta, rooms);
        return new Gson().toJson(rjson);
    }
}
