package net.ilgrim.hotels.api.routes.rooms.category;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.RoomCategory;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class CategoriesGet extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public CategoriesGet(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        final Cursor cursor = database.query("SELECT id, category FROM category_room;");

        final ArrayList<RoomCategory> roomCategories = ModelCreator.create(new RoomCategory(), cursor);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<RoomCategory>> rjson = new Rjson<>(meta, roomCategories);
        return new Gson().toJson(rjson);
    }
}
