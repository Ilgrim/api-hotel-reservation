package net.ilgrim.hotels.api.routes.rooms.category;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.RoomCategory;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class CategoryPatch extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public CategoryPatch(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            Integer.parseInt(request.params("id"));
        } catch (NumberFormatException e) {
            return incorrectData("id");
        }

        final int categoryId = Integer.parseInt(request.params("id"));

        if (categoryId == 0) {
            return zeeroInId();
        }

        if (request.queryParams().size() < 1) return emptyField();

        final RoomCategory roomCategory = new RoomCategory();
        roomCategory.setCategory(URLDecoder.decode(request.queryParams("category"), "UTF-8"));

        final Cursor updateCategory = database.updateWithReturnCursor("UPDATE category_room set category = ? where id = ? RETURNING *;", roomCategory.getCategory(), categoryId);
        if (updateCategory.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<RoomCategory> roomCategories = ModelCreator.create(new RoomCategory(), updateCategory);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<RoomCategory>> rjson = new Rjson<>(meta, roomCategories);
        return new Gson().toJson(rjson);
    }
}
