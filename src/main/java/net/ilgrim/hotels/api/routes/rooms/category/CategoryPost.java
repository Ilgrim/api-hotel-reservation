package net.ilgrim.hotels.api.routes.rooms.category;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.RoomCategory;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class CategoryPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public CategoryPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {
        if (request.queryParams().size() < 1) return emptyField();

        final RoomCategory roomCategory = new RoomCategory();
        roomCategory.setCategory(URLDecoder.decode(request.queryParams("category"), "UTF-8"));

        final Cursor checkCategory = database.query("SELECT id, category FROM category_room WHERE lower(category) = lower(?);", roomCategory.getCategory());
        if (checkCategory.size() > 0) {
            return dataContains();
        }

        final Cursor newCategory = database.insertWithReturnCursor("INSERT INTO category_room (category) VALUES (?) RETURNING *;", roomCategory.getCategory());
        if (newCategory.size() == 0) {
            return errorWritingDatabase();
        }

        final ArrayList<RoomCategory> roomCategories = ModelCreator.create(new RoomCategory(), newCategory);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<RoomCategory>> rjson = new Rjson<>(meta, roomCategories);
        return new Gson().toJson(rjson);
    }
}
