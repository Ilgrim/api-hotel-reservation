package net.ilgrim.hotels.api.routes.rooms.comforts.comfort;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.routes.Comfort;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.util.ModelCreator;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URLDecoder;
import java.util.ArrayList;

public class RoomComfortPost extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public RoomComfortPost(DatabaseHelperExtended database) {
        this.database = database;
    }

    @Override
    public String handle(Request request, Response response) throws Exception {

        final String comfortTitle = URLDecoder.decode(request.queryParams("title"), "UTF-8");
        if (comfortTitle.equals("")) {
            return emptyField();
        }

        final Cursor checkData = database.query("SELECT id, title FROM comforts_room WHERE lower(title) = lower(?);", comfortTitle);

        if (checkData.size() > 0) {
            return dataContains();
        }

        final int comfortId = database.insertWithReturnId("INSERT INTO comforts_room (title) VALUES (?);", comfortTitle);

        if (comfortId == 0) {
            return errorWritingDatabase();
        }

        final Cursor dataRespons = database.query("SELECT id, title FROM comforts_room WHERE id = ?", comfortId);

        if (dataRespons.size() == 0) {
            return errorReadDatabase();
        }

        final ArrayList<Comfort> comforts = ModelCreator.create(new Comfort(), dataRespons);

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Comfort>> rjson = new Rjson<>(meta, comforts);
        return new Gson().toJson(rjson);
    }
}