package net.ilgrim.hotels.api.routes.scheme;

import com.google.gson.Gson;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.Rjson;
import net.ilgrim.hotels.api.models.Status;
import net.ilgrim.hotels.api.models.meta.Meta;
import net.ilgrim.hotels.api.models.scheme.Scheme;
import net.ilgrim.hotels.api.routes.RouteResponses;
import net.ilgrim.hotels.api.routes.scheme.routes.*;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

/**
 * Created by ilgrim.
 */
public class SchemeRoute extends RouteResponses implements Route {
    private final DatabaseHelperExtended database;

    public SchemeRoute(DatabaseHelperExtended database) {
        this.database = database;
    }
    @Override
    public String handle(Request request, Response response) throws Exception {
        final ArrayList<Scheme> schemes = new ArrayList<>();

        final LocationScheme locationScheme = new LocationScheme();
        final PlacementScheme placementScheme = new PlacementScheme();
        final ComfortHotelScheme comfortHotelScheme = new ComfortHotelScheme();
        final PhotoHotelScheme photoHotelScheme = new PhotoHotelScheme(database);
        final HotelCoordinatesScheme hotelCoordinatesScheme = new HotelCoordinatesScheme(database);
        final HotelCommentScheme hotelCommentScheme = new HotelCommentScheme(database);
        final HotelScheme hotelScheme = new HotelScheme(database);
        final PeriodScheme periodScheme = new PeriodScheme(database);
        final RoomCategoryScheme roomCategoryScheme = new RoomCategoryScheme();
        final PhotoRoomScheme photoRoomScheme = new PhotoRoomScheme(database);
        final ComfortRoomScheme comfortRoomScheme = new ComfortRoomScheme();
        final RoomScheme roomScheme = new RoomScheme(database);
        final PriceScheme priceScheme = new PriceScheme(database);
        final CHotelScheme cHotelScheme = new CHotelScheme(database);
        final CRoomScheme cRoomScheme = new CRoomScheme(database);

        schemes.add(locationScheme.get());
        schemes.add(placementScheme.get());
        schemes.add(comfortHotelScheme.get());
        schemes.add(photoHotelScheme.get());
        schemes.add(hotelCoordinatesScheme.get());
        schemes.add(hotelCommentScheme.get());
        schemes.add(hotelScheme.get());
        schemes.add(periodScheme.get());
        schemes.add(roomCategoryScheme.get());
        schemes.add(photoRoomScheme.get());
        schemes.add(comfortRoomScheme.get());
        schemes.add(roomScheme.get());
        schemes.add(priceScheme.get());
        schemes.add(cHotelScheme.get());
        schemes.add(cRoomScheme.get());

        final Status status = new Status(200, "OK");
        final Meta meta = new Meta(status);
        final Rjson<ArrayList<Scheme>> rjson = new Rjson<>(meta, schemes);
        return new Gson().toJson(rjson);
    }
}
