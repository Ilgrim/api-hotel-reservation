package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class CHotelScheme {
    private final DatabaseHelperExtended database;

    public CHotelScheme(DatabaseHelperExtended database) {
        this.database = database;
    }

    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Comforts in Hotels");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("insert", "POST::/api/v1/comforts/in/hotel");
        methods.put("get", "GET::/api/v1/comforts/in/hotel");
        methods.put("delete", "DELETE::/api/v1/comforts/in/hotel/:id");
        methods.put("update", "PATCH::/api/v1/comforts/in/hotel/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> hotel_id = new HashMap<>();
        hotel_id.put("name", "hotel_id");
        hotel_id.put("label", "hotel_id");
        hotel_id.put("type", "select");
        hotel_id.put("select", getHotelsSelect());
        fields.add(hotel_id);
        final HashMap<String, Object> comfort_id = new HashMap<>();
        comfort_id.put("name", "comfort_id");
        comfort_id.put("label", "comfort_id");
        comfort_id.put("type", "select");
        comfort_id.put("select", getComfortsSelect());
        fields.add(comfort_id);

        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getHotelsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotels;");
        final HashMap<String, Integer> hotels = new HashMap<>();

        while (cursor.next()) {
            hotels.put(cursor.getString("title").trim(), cursor.getInt("id", 0));
        }

        return hotels;
    }

    private HashMap<String, Integer> getComfortsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM comforts;");
        final HashMap<String, Integer> comforts = new HashMap<>();
        while (cursor.next()) {
            comforts.put(cursor.getString("title").trim(), cursor.getInt("id", 0));
        }

        return comforts;
    }
}
