package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class CRoomScheme {
    private final DatabaseHelperExtended database;

    public CRoomScheme(DatabaseHelperExtended database) {
        this.database = database;
    }

    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Comforts in Room");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("insert", "POST::/api/v1/comforts/in/room");
        methods.put("get", "GET::/api/v1/comforts/in/room");
        methods.put("delete", "DELETE::/api/v1/comforts/in/room/:id");
        methods.put("update", "PATCH::/api/v1/comforts/in/room/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> room_id = new HashMap<>();
        room_id.put("name", "room_id");
        room_id.put("label", "room_id");
        room_id.put("type", "select");
        room_id.put("select", getRoomsSelect());
        fields.add(room_id);
        final HashMap<String, Object> comfort_room_id = new HashMap<>();
        comfort_room_id.put("name", "comfort_room_id");
        comfort_room_id.put("label", "comfort_room_id");
        comfort_room_id.put("type", "select");
        comfort_room_id.put("select", getRoomComfortsSelect());
        fields.add(comfort_room_id);

        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getRoomsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotel_room;");
        final HashMap<String, Integer> rooms = new HashMap<>();

        while (cursor.next()) {
            rooms.put(cursor.getString("title").trim(), cursor.getInt("id", 0));
        }

        return rooms;
    }

    private HashMap<String, Integer> getRoomComfortsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM comforts_room;");
        final HashMap<String, Integer> comforts = new HashMap<>();
        while (cursor.next()) {
            comforts.put(cursor.getString("title").trim(), cursor.getInt("id", 0));
        }

        return comforts;
    }
}
