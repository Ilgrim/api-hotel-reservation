package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class HotelCommentScheme {
    private final DatabaseHelperExtended database;

    public HotelCommentScheme(DatabaseHelperExtended database) {
        this.database = database;
    }

    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Hotel Comments");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/hotels/comments");
        methods.put("insert", "POST::/api/v1/hotel/:hotel_id/comment");
        methods.put("get", "GET::/api/v1/hotel/:hotel_id/comment");
        methods.put("delete", "DELETE::/api/v1/comment/:id");
        methods.put("update", "PATCH::/api/v1/comment/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> hotelId = new HashMap<>();
        hotelId.put("name", "hotel_id");
        hotelId.put("label", "hotel_id");
        hotelId.put("type", "select");
        hotelId.put("select", getHotelsSelect());
        fields.add(hotelId);
        final HashMap<String, Object> assessment = new HashMap<>();
        assessment.put("name", "assessment");
        assessment.put("label", "assessment");
        assessment.put("type", "int");
        fields.add(assessment);
        final HashMap<String, Object> author = new HashMap<>();
        author.put("name", "author");
        author.put("label", "author");
        author.put("type", "text");
        fields.add(author);
        final HashMap<String, Object> comment = new HashMap<>();
        comment.put("name", "comment");
        comment.put("label", "comment");
        comment.put("type", "textarea");
        fields.add(comment);
        final HashMap<String, Object> date = new HashMap<>();
        date.put("name", "date");
        date.put("label", "date");
        date.put("type", "date");
        date.put("mask", "YYYY-MM-DD HH:mm:00");
        fields.add(date);

        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getHotelsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotels;");
        final HashMap<String, Integer> hotels = new HashMap<>();

        while (cursor.next()) {
            hotels.put(cursor.getString("title"), cursor.getInt("id", 0));
        }

        return hotels;
    }
}
