package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;
import net.ilgrim.hotels.api.routes.RouteResponses;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class HotelScheme extends RouteResponses {
    private final DatabaseHelperExtended database;

    public HotelScheme(DatabaseHelperExtended database) {
        this.database = database;
    }

    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Hotels");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/hotels");
        methods.put("insert", "POST::/api/v1/hotel");
        methods.put("get", "GET::/api/v1/hotel/:id");
        methods.put("delete", "DELETE::/api/v1/hotel/:id");
        methods.put("update", "PATCH::/api/v1/hotel/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> title = new HashMap<>();
        title.put("name", "title");
        title.put("label", "title");
        title.put("type", "text");
        fields.add(title);
        final HashMap<String, Object> description = new HashMap<>();
        description.put("name", "description");
        description.put("label", "description");
        description.put("type", "textarea");
        fields.add(description);
        final HashMap<String, Object> location_id = new HashMap<>();
        location_id.put("name", "location_id");
        location_id.put("label", "location_id");
        location_id.put("type", "select");
        location_id.put("select", getLocationSelect());
        fields.add(location_id);
        final HashMap<String, Object> type_pacement_id = new HashMap<>();
        type_pacement_id.put("name", "type_placement_id");
        type_pacement_id.put("label", "type_placement_id");
        type_pacement_id.put("type", "select");
        type_pacement_id.put("select", getPlacementSelect());
        fields.add(type_pacement_id);



        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getLocationSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM locations;");
        final HashMap<String, Integer> locations = new HashMap<>();

        while (cursor.next()) {
            locations.put(cursor.getString("title"), cursor.getInt("id", 0));
        }

        return locations;
    }

    private HashMap<String, Integer> getPlacementSelect() {
        final Cursor cursor = database.query("SELECT id, placement FROM type_placement;");
        final HashMap<String, Integer> placements = new HashMap<>();
        while (cursor.next()) {
            placements.put(cursor.getString("placement"), cursor.getInt("id", 0));
        }

        return placements;
    }
}
