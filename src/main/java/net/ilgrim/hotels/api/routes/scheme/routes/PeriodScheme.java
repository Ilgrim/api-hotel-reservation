package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class PeriodScheme {
    private final DatabaseHelperExtended database;

    public PeriodScheme(DatabaseHelperExtended database) {
        this.database = database;
    }
    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Periods");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/hotels/period");
        methods.put("insert", "POST::/api/v1/hotel/:hotel_id/period");
        methods.put("get", "GET::/api/v1/hotel/:hotel_id/period");
        methods.put("delete", "DELETE::/api/v1/hotel/period/:id");
        methods.put("update", "PATCH::/api/v1/hotel/:hotel_id/period/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> hotelId = new HashMap<>();
        hotelId.put("name", "hotel_id");
        hotelId.put("label", "hotel_id");
        hotelId.put("type", "select");
        hotelId.put("select", getHotelsSelect());
        fields.add(hotelId);
        final HashMap<String, Object> title_period = new HashMap<>();
        title_period.put("name", "title_period");
        title_period.put("label", "title_period");
        title_period.put("type", "text");
        fields.add(title_period);
        final HashMap<String, Object> period_start = new HashMap<>();
        period_start.put("name", "period_start");
        period_start.put("label", "period_start");
        period_start.put("type", "date");
        period_start.put("mask", "YYYY-MM-DD HH:mm:00");
        fields.add(period_start);
        final HashMap<String, Object> period_end = new HashMap<>();
        period_end.put("name", "period_end");
        period_end.put("label", "period_end");
        period_end.put("type", "date");
        period_end.put("mask", "YYYY-MM-DD HH:mm:00");
        fields.add(period_end);

        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getHotelsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotels;");
        final HashMap<String, Integer> hotels = new HashMap<>();

        while (cursor.next()) {
            hotels.put(cursor.getString("title"), cursor.getInt("id", 0));
        }

        return hotels;
    }
}
