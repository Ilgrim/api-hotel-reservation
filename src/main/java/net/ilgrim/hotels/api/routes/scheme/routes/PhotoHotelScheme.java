package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class PhotoHotelScheme {
    private final DatabaseHelperExtended database;

    public PhotoHotelScheme(DatabaseHelperExtended database) {
        this.database = database;
    }

    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Photo in Hotel");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/hotels/photo");
        methods.put("get", "GET::/api/v1/hotel/:hotel_id/photo");
        methods.put("insert", "POST::/api/v1/hotel/:hotel_id/photo");
        methods.put("delete", "DELETE::/api/v1/hotel/photo/:id");
        methods.put("update", "PATCH::/api/v1/hotel/:hotel_id/photo/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> hotelId = new HashMap<>();
        hotelId.put("name", "hotel_id");
        hotelId.put("label", "hotel_id");
        hotelId.put("type", "select");
        hotelId.put("select", getHotelsSelect());
        fields.add(hotelId);
        final HashMap<String, Object> photoUrl = new HashMap<>();
        photoUrl.put("name", "photo_url");
        photoUrl.put("label", "photo_url");
        photoUrl.put("type", "text");
        fields.add(photoUrl);

        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getHotelsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotels;");
        final HashMap<String, Integer> hotels = new HashMap<>();

        while (cursor.next()) {
            hotels.put(cursor.getString("title"), cursor.getInt("id", 0));
        }

        return hotels;
    }
}
