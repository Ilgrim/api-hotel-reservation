package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class PhotoRoomScheme {
    private final DatabaseHelperExtended database;

    public PhotoRoomScheme(DatabaseHelperExtended database) {
        this.database = database;
    }
    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Photo in Room");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/rooms/photo");
        methods.put("insert", "POST::/api/v1/room/:room_id/photo");
        methods.put("get", "GET::/api/v1/room/:room_id/photo");
        methods.put("delete", "DELETE::/api/v1/room/photo/:id");
        methods.put("update", "PATCH::/api/v1/room/:room_id/photo/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> roomId = new HashMap<>();
        roomId.put("name", "room_id");
        roomId.put("label", "room_id");
        roomId.put("type", "select");
        roomId.put("select", getRoomsSelect());
        fields.add(roomId);
        final HashMap<String, Object> photoUrl = new HashMap<>();
        photoUrl.put("name", "photo_url");
        photoUrl.put("label", "photo_url");
        photoUrl.put("type", "text");
        fields.add(photoUrl);

        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getRoomsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotel_room;");
        final HashMap<String, Integer> hotels = new HashMap<>();

        while (cursor.next()) {
            hotels.put(cursor.getString("title"), cursor.getInt("id", 0));
        }

        return hotels;
    }
}
