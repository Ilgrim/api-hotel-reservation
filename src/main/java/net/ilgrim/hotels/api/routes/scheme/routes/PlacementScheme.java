package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class PlacementScheme {
    public Scheme get() {
        final Scheme scheme = new Scheme();
        scheme.setTitleInset("Placement");

        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/placements");
        methods.put("insert", "POST::/api/v1/placement");
        methods.put("get", "GET::/api/v1/placement/:id");
        methods.put("delete", "DELETE::/api/v1/placement/:id");
        methods.put("update", "PATCH::/api/v1/placement/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> title = new HashMap<>();
        title.put("name", "placement");
        title.put("label", "placement");
        title.put("type", "text");
        fields.add(title);

        scheme.setFields(fields);

        return scheme;
    }
}
