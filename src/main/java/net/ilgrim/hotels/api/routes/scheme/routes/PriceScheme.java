package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class PriceScheme {
    private final DatabaseHelperExtended database;

    public PriceScheme(DatabaseHelperExtended database) {
        this.database = database;
    }

    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Prices");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/room/period/prices");
        methods.put("insert", "POST::/api/v1/room/:room_id/period/:period_id/price");
        methods.put("get", "GET::/api/v1/room/period/price/:id");
        methods.put("delete", "DELETE::/api/v1/room/period/price/:id");
        methods.put("update", "PATCH::/api/v1/room/:room_id/period/:period_id/price/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> period_id = new HashMap<>();
        period_id.put("name", "period_id");
        period_id.put("label", "period_id");
        period_id.put("type", "select");
        period_id.put("select", getPeriodSelect());
        fields.add(period_id);
        final HashMap<String, Object> room_id = new HashMap<>();
        room_id.put("name", "room_id");
        room_id.put("label", "room_id");
        room_id.put("type", "select");
        room_id.put("select", getRoomsSelect());
        fields.add(room_id);
        final HashMap<String, Object> cost_standart = new HashMap<>();
        cost_standart.put("name", "cost_standart");
        cost_standart.put("label", "cost_standart");
        cost_standart.put("type", "int");
        fields.add(cost_standart);
        final HashMap<String, Object> cost_child = new HashMap<>();
        cost_child.put("name", "cost_child");
        cost_child.put("label", "cost_child");
        cost_child.put("type", "int");
        fields.add(cost_child);
        final HashMap<String, Object> cost_extra_standart = new HashMap<>();
        cost_extra_standart.put("name", "cost_extra_standart");
        cost_extra_standart.put("label", "cost_extra_standart");
        cost_extra_standart.put("type", "int");
        fields.add(cost_extra_standart);
        final HashMap<String, Object> cost_extra_child = new HashMap<>();
        cost_extra_child.put("name", "cost_extra_child");
        cost_extra_child.put("label", "cost_extra_child");
        cost_extra_child.put("type", "int");
        fields.add(cost_extra_child);
        final HashMap<String, Object> day_off_cost_standart = new HashMap<>();
        day_off_cost_standart.put("name", "day_off_cost_standart");
        day_off_cost_standart.put("label", "day_off_cost_standart");
        day_off_cost_standart.put("type", "int");
        fields.add(day_off_cost_standart);
        final HashMap<String, Object> day_off_cost_child = new HashMap<>();
        day_off_cost_child.put("name", "day_off_cost_child");
        day_off_cost_child.put("label", "day_off_cost_child");
        day_off_cost_child.put("type", "int");
        fields.add(day_off_cost_child);
        final HashMap<String, Object> day_off_cost_extra_standart = new HashMap<>();
        day_off_cost_extra_standart.put("name", "day_off_cost_extra_standart");
        day_off_cost_extra_standart.put("label", "day_off_cost_extra_standart");
        day_off_cost_extra_standart.put("type", "int");
        fields.add(day_off_cost_extra_standart);
        final HashMap<String, Object> day_off_cost_extra_child = new HashMap<>();
        day_off_cost_extra_child.put("name", "day_off_cost_extra_child");
        day_off_cost_extra_child.put("label", "day_off_cost_extra_child");
        day_off_cost_extra_child.put("type", "int");
        fields.add(day_off_cost_extra_child);
        final HashMap<String, Object> stock = new HashMap<>();
        stock.put("name", "stock");
        stock.put("label", "stock");
        stock.put("type", "bool");
        fields.add(stock);

        scheme.setFields(fields);

        return scheme;
    }

    public HashMap<String, Integer> getPeriodSelect() {

        final Cursor cursor = database.query("SELECT id, title_period FROM periods;");
        final HashMap<String, Integer> periods = new HashMap<>();

        while (cursor.next()) {
            periods.put(cursor.getString("title_period"), cursor.getInt("id", 0));
        }

        return periods;
    }

    private HashMap<String, Integer> getRoomsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotel_room;");
        final HashMap<String, Integer> rooms = new HashMap<>();

        while (cursor.next()) {
            rooms.put(cursor.getString("title"), cursor.getInt("id", 0));
        }

        return rooms;
    }
}
