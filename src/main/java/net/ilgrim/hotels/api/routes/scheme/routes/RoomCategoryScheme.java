package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class RoomCategoryScheme {
    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Room Categories");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/categories");
        methods.put("insert", "POST::/api/v1/category");
        methods.put("delete", "DELETE::/api/v1/category/:id");
        methods.put("update", "PATCH::/api/v1/category/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> title = new HashMap<>();
        title.put("name", "category");
        title.put("label", "category");
        title.put("type", "text");
        fields.add(title);

        scheme.setFields(fields);

        return scheme;
    }
}
