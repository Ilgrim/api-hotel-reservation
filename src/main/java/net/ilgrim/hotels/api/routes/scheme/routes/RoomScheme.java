package net.ilgrim.hotels.api.routes.scheme.routes;

import net.ilgrim.hotels.api.Database.Cursor;
import net.ilgrim.hotels.api.Database.DatabaseHelperExtended;
import net.ilgrim.hotels.api.models.scheme.Scheme;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ilgrim.
 */
public class RoomScheme {
    private final DatabaseHelperExtended database;

    public RoomScheme(DatabaseHelperExtended database) {
        this.database = database;
    }

    public Scheme get() {
        final Scheme scheme = new Scheme();

        scheme.setTitleInset("Rooms");
        final HashMap<String, String> methods = new HashMap<>();
        methods.put("list", "GET::/api/v1/rooms");
        methods.put("hotel_list", "GET::/api/v1/hotel/:hotel_id/rooms");
        methods.put("insert", "POST::/api/v1/room");
        methods.put("get", "GET::/api/v1/room/:id");
        methods.put("delete", "DELETE::/api/v1/room/:id");
        methods.put("update", "PATCH::/api/v1/room/:id");
        scheme.setMethods(methods);

        final ArrayList<HashMap<String, Object>> fields = new ArrayList<>();

        final HashMap<String, Object> id = new HashMap<>();
        id.put("name", "id");
        id.put("label", "id");
        id.put("type", "auto");
        fields.add(id);
        final HashMap<String, Object> title = new HashMap<>();
        title.put("name", "title");
        title.put("label", "title");
        title.put("type", "text");
        fields.add(title);
        final HashMap<String, Object> description = new HashMap<>();
        description.put("name", "description");
        description.put("label", "description");
        description.put("type", "textarea");
        fields.add(description);
        final HashMap<String, Object> hotelId = new HashMap<>();
        hotelId.put("name", "hotel_id");
        hotelId.put("label", "hotel_id");
        hotelId.put("type", "select");
        hotelId.put("select", getHotelsSelect());
        fields.add(hotelId);
        final HashMap<String, Object> numberBeds = new HashMap<>();
        numberBeds.put("name", "number_beds");
        numberBeds.put("label", "number_beds");
        numberBeds.put("type", "int");
        fields.add(numberBeds);
        final HashMap<String, Object> numberExtraBeds = new HashMap<>();
        numberExtraBeds.put("name", "number_extra_beds");
        numberExtraBeds.put("label", "number_extra_beds");
        numberExtraBeds.put("type", "int");
        fields.add(numberExtraBeds);
        final HashMap<String, Object> categoryId = new HashMap<>();
        categoryId.put("name", "category_id");
        categoryId.put("label", "category_id");
        categoryId.put("type", "select");
        categoryId.put("select", getCategories());
        fields.add(categoryId);

        scheme.setFields(fields);

        return scheme;
    }

    private HashMap<String, Integer> getCategories() {
        final Cursor cursor = database.query("SELECT id, category FROM category_room;");
        final HashMap<String, Integer> categories = new HashMap<>();

        while (cursor.next()) {
            categories.put(cursor.getString("category"), cursor.getInt("id", 0));
        }

        return categories;
    }

    private HashMap<String, Integer> getHotelsSelect() {
        final Cursor cursor = database.query("SELECT id, title FROM hotels;");
        final HashMap<String, Integer> hotels = new HashMap<>();

        while (cursor.next()) {
            hotels.put(cursor.getString("title"), cursor.getInt("id", 0));
        }

        return hotels;
    }
}
