package net.ilgrim.hotels.api.util;

import net.ilgrim.hotels.api.Database.Cursor;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;

public class ModelCreator {

    public static <M> ArrayList<M> create(M model, Cursor cursor) throws IOException {

        try {
            final Class<?> cls = model.getClass();

            final ArrayList<M> result = new ArrayList<M>();
            while (cursor.next()) {

                final Class<?> item = Class.forName(cls.getName());
                final M obj = (M) item.newInstance();

                final Field[] fields = item.getDeclaredFields();
                for (Field field : fields) {

                    field.setAccessible(true);

                    final Class<?> type = field.getType();
                    final String field_name = field.getName();

                    //System.out.println("Type: "+type.toString());

                    switch (type.toString()) {

                        case "int": {
                            final int value = cursor.getInt(field_name, 0);
                            field.setInt(obj, value);
                            break;
                        }

                        case "float": {
                            final float value = cursor.getFloat(field_name, 0);
                            field.setFloat(obj, value);
                            break;
                        }

                        case "long": {
                            final long value = cursor.getLong(field_name, 0);
                            field.setFloat(obj, value);
                            break;
                        }

                        case "boolean": {
                            final boolean value = cursor.getBoolean(field_name, false);
                            field.setBoolean(obj, value);
                            break;
                        }

                        case "class java.lang.String": {
                            final String value = cursor.getString(field_name);
                            field.set(obj, value);
                            break;
                        }

                        case "class java.sql.Timestamp": {
                            final Timestamp value = cursor.getTimestamp(field_name, "1970-01-01 00:00:000");
                            field.set(obj, value);
                            break;
                        }

                        default:
                            throw new IOException();
                    }
                }

                result.add(obj);
            }

            return result;
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new IOException();
        }
    }
}
