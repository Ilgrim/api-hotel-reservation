package net.ilgrim.hotels.api.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ilgrim.
 */
public class Util {
    public static String sha1(String target) {
        final char digit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        try {
            final MessageDigest sh = MessageDigest.getInstance("SHA1");
            sh.update(target.getBytes());

            final StringBuilder sb = new StringBuilder();
            for (byte item : sh.digest()) {
                sb.append(digit[(item >> 4) & 0x0f]);
                sb.append(digit[item & 0x0f]);
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
